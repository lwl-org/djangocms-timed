=========
Changelog
=========


1.1.0 (2024-07-23)
==================

* Add support for Django 4.0
* Replace usage of ``ugettext_lazy`` and ``force_text``


1.0.1 (2024-03-11)
==================

* Add  support for Django 2
* Fix show timed content if edit in url


1.0.0 (2016-08-22)
==================

* Second version of the app completely rewritten; backwards incompatible

